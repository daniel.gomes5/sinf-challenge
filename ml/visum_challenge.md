# SMARTEX.ai VISUM 2022 Challenge.

## Description

To start the test first checkout the following [folder](https://drive.google.com/drive/folders/1QxKjj_vWJeHU6LdAm_Z5_4VHVTSin_QT?usp=sharing). In it, you will find 2 files and one folder:
 * `frames`: a folder containing 3676 JPEG images;
 * `data.csv`: a `.csv` file `(3676, 2)` with the first column relating to the file names in the 
   `frames` folder and the `Label` to the corresponding image label ;
 * `hint-*.md`: multiple `.md` files with hints that make the resolution somewhat easier you are free to use them as you please. 
   
So, this dataset is composed of `N = 3676` samples. Around 1% of this dataset has been mislabeled by someone in our team.

Now, there are 4 kinds of samples:
 * `NoDefect`: clean fabric;
 * `Needle`: a defect that looks like a vertical, thin, dark line;
 * `ThinYarn`: a defect that looks like horizontal, periodic;
 * `ThickYarn`: a defect that looks like horizontal, periodic, bright lines;
   
Your task is then to try and identify which of these frames are mislabeled. Although this is a small
dataset, it is part of the exercise to employ a method that would work in a much larger dataset.

Feel free to ask for any clarifications during the duration of the test.


## Deliverable

 * A `.csv` file of strings containing the names of the frames you 
   identified as being mislabeled. Please send this via email to: daniel.gomes@smartex.ai.
   Your performance will be calculated using [Matthews Correlation Coeficient](https://en.wikipedia.org/wiki/Phi_coefficient).