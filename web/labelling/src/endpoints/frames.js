import axiosService from "../services/axiosService";

const baseUrl = "/frames";

export const getFrames = (numberOfFrames) => {
  return axiosService.get(baseUrl, { params: { n: numberOfFrames } });
};
