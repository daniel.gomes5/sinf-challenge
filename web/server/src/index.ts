import express, { RequestHandler } from 'express';
import { env } from './environment';
import routes from './routes/index';
import { PrismaClient } from '@prisma/client';
import cors from 'cors';

const app = express();
const port = env.PORT;
const prisma = new PrismaClient();

app.use(express.json({ limit: '10mb' }) as RequestHandler);
app.use(express.urlencoded({ extended: true }) as RequestHandler);
app.use(cors({ origin: env.WHITELIST_ORIGINS, credentials: true }));
app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

app.use('/', routes(prisma));
