import { PrismaClient, Prisma, light_type_enum, position_type_enum } from '@prisma/client';
import labels from './labels.json';
import frames from './frames.json';

const prisma = new PrismaClient();

async function main() {
  console.log(`Start seeding ...`);

  await prisma.labels.createMany({
    data: labels,
  });

  await prisma.frames.createMany({
    data: frames.map(({ light, position, ...restOfTheFrame }) => ({
      ...restOfTheFrame,
      light: { IR: light_type_enum.IR, UV: light_type_enum.UV, VIS: light_type_enum.VIS }[
        light
      ] as light_type_enum,
      position: {
        TOP: position_type_enum.TOP,
        FRONT: position_type_enum.FRONT,
      }[position] as position_type_enum,
    })),
  });

  console.log(`Seeding finished.`);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
