# SMARTEX BACKEND CHALLENGE

Challenge for SINF participants that are visiting SMARTEX Porto office at 30/10/2023.

## Description

Node application using typescript node. This app is responsible for connect the user to the database storage containing all the information to create classifications, data that connects labels with images, which will feed the models.

## Already implemented

- Endpoint to retrieve all frames existing in the database
- Endpoint to retrieve all labels existing in the database

Regarding the database, the table `frames` and `labels` is already created.

Check the first [migration](prisma/migrations/20231030174359_init/migration.sql)

## Goals

- Allow the user to save/create `Classifications`
- Allow the user to find `Classifications`
- Allow user to save new `Frames`
- Allow user to save/create `Labels`

## Installation

Follow the instructions on [README](../README)

## Observations 

You can see on [.env](.env) the user created in the database is `test`. If you change this user, you will need to change the database URL connection in this same file. 

## License

[MIT](https://choosealicense.com/licenses/mit/)
