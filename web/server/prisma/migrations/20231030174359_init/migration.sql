-- CreateEnum
CREATE TYPE "light_type_enum" AS ENUM ('IR', 'VIS', 'UV');

-- CreateEnum
CREATE TYPE "position_type_enum" AS ENUM ('TOP', 'FRONT');

-- CreateTable
CREATE TABLE "frames" (
    "id" UUID NOT NULL DEFAULT gen_random_uuid(),
    "key" VARCHAR NOT NULL,
    "creation_timestamp" TIMESTAMPTZ(3) NOT NULL,
    "light" "light_type_enum" NOT NULL,
    "position" "position_type_enum" NOT NULL,
    "camera" INTEGER,
    "url" VARCHAR,

    CONSTRAINT "frames_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "labels" (
    "id" INTEGER NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "parent_id" INTEGER,

    CONSTRAINT "labels_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "frames_key_key" ON "frames"("key");

-- CreateIndex
CREATE INDEX "frames_temp_idx" ON "frames"("key");

-- AddForeignKey
ALTER TABLE "labels" ADD CONSTRAINT "labels_parent_label_id_fkey" FOREIGN KEY ("parent_id") REFERENCES "labels"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
